# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Este es el repositorio GIT correspondiente al problema 3

El proyecto fue construido utilizando Spring Boot como tecnología base y el IDE Spring Tool Suite 4

El proyecto fue diseñado para funcionar utilizando una base de datos MySQL


### How do I get set up? ###
* Crear base de datos en un servidor MySQL 
* Ejecutar la tarea de Maven "install" para que se genere el war de la aplicación
* Configurar el archivo \WEB-INF\classes\application.properties con los datos de conexión a la base de datos MySQL 
* Copiar el war generado en "target/exercisethree.war" hacia un Tomcat 8.5
* Una vez levantado completamente el Tomcat8.5 se debe ir la URL equivalente a: http://localhost:8080/exercisethree/mainpage  


### Contribution guidelines ###



### Who do I talk to? ###
* Lester Hernandez (lestercujae@gmail.com)
