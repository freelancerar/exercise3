/**
 * Aux javascript scripts
 */

$(document).ready(function () {

    $("#submitBTN").click(function (event) {        
        event.preventDefault();

        uploadFile();

    });

    $("#retrieveBTN").click(function (event) {        
        retrieve();
    });
    
});

//functions for uploadind a file
function uploadFile() {
    var form = $('#mainForm')[0];

    var data = new FormData(form);    

    $("#submitBTN").prop("disabled", true);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "../exercisethree/uploadfile",
        //url: "../uploadfile",
        data: data,
        
        processData: false, 
        contentType: false,
        cache: false,        
        success: function (data) {
            $("#resultLabel").text(data);
            
            console.log("SUCCESS : ", data);
            
            $("#submitBTN").prop("disabled", false);

            $("#fileInput").val('');
            
        },
        error: function (e) {
            $("#resultLabel").text(e.responseText);
            
            console.log("ERROR : ", e);
            
            $("#submitBTN").prop("disabled", false);

        }
    });

}

//functions for retrieving the stored data
function retrieve() {
    $.ajax({
        type: "GET",        
        url: "../exercisethree/storedFileProcessing",
        //url: "../storedFileProcessing",
        success: function (data) {
        	var newTRsHTML = '';
        	
            $.each(data, function (i, item) {
            	newTRsHTML += '<tr><td>' + item.fileName + '</td><td>' + item.fileValue + '</td><td>' + item.processDate + '</td></tr>';
            });
            
            $("#resultsTable").find("tr:gt(0)").remove();
            
            $('#resultsTable').append(newTRsHTML);        	           
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });

}