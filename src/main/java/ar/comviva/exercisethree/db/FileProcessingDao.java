package ar.comviva.exercisethree.db;

import java.util.List;

import ar.comviva.exercisethree.orm.FileProcessing;

public interface FileProcessingDao {
	
	/**
	 * @param newProcessing new object
	 */
	void insert(FileProcessing newProcessing);
	
	
	/**
	 * @return all the file processing records
	 */
	List<FileProcessing> getAll();
	
}
