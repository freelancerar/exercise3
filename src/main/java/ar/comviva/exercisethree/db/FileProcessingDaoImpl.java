package ar.comviva.exercisethree.db;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Component;

import ar.comviva.exercisethree.orm.FileProcessing;

@Component
public class FileProcessingDaoImpl extends JdbcDaoSupport implements FileProcessingDao {
	@Autowired 
	DataSource dataSource;
	
	@PostConstruct
	private void initialize(){
		setDataSource(dataSource);
	}
	
	@Override
	public void insert(FileProcessing newProcessing) {
		checkIfTableExists();				
		
		String insertQuery = "INSERT INTO file_processing (file_processing_id, file_name, file_value, process_date) VALUES (?,?,?,?)";
		
		getJdbcTemplate().update(insertQuery, new Object[]{newProcessing.getId(), newProcessing.getFileName(), 
				                                           new BigDecimal(newProcessing.getFileValue()), newProcessing.getProcessDate()});
	}

	@Override
	public List<FileProcessing> getAll() {
		String sql = "SELECT * FROM file_processing ORDER BY process_date";
		
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
		
		List<FileProcessing> result = new ArrayList<FileProcessing>();
		
		for(Map<String, Object> row:rows){
			FileProcessing processing = new FileProcessing();
			
			processing.setId((String)row.get("file_processing_id"));
			processing.setFileName((String)row.get("file_name"));
			processing.setFileValue(((Float) row.get("file_value")).floatValue());
			processing.setProcessDate((Timestamp)row.get("process_date"));
			
			result.add(processing);
		}
		
		return result;
	}

	/**
	 * Checks if the core table of the exercise exists in the database
	 * @param conn JDBC Connection
	 * @throws SQLException 
	 */
	private void checkIfTableExists() {
		String existTableQuery = "SELECT * " + 
				  				 "FROM information_schema.tables " +
				  				 "WHERE table_schema = ? AND table_name = 'file_processing' " +
				  				 "LIMIT 1";
		String database = "";
		
		try {
			String connection = getJdbcTemplate().getDataSource().getConnection().getMetaData().getURL().toString();
			
			String url = "jdbc:mysql://localhost:3306/exercise2";
			
			int pos = url.lastIndexOf('/');
			
			database = url.substring(pos + 1);
			
		} catch (SQLException e) {
		}
		
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(existTableQuery, new Object[]{database});
		
		if(rows.isEmpty()) {//it's necessary create the table
			String createTableQuery = "CREATE TABLE file_processing (" +
										  	"file_processing_id  varchar(32) NOT NULL, " +
										  	"file_name  varchar(255) NOT NULL, " +
										  	"file_value  float NOT NULL, " +
										  	"process_date  timestamp NOT NULL" +
										 ")";

			getJdbcTemplate().execute(createTableQuery);
			
			String addPrimaryKeyQuery = "ALTER TABLE file_processing " + 
				     					"ADD CONSTRAINT PK_FILEPROCESSINGID PRIMARY KEY (file_processing_id)";
			
			getJdbcTemplate().execute(addPrimaryKeyQuery);
		}				
	}
}
