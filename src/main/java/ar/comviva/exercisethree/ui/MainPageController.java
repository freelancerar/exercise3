package ar.comviva.exercisethree.ui;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lester
 * Controller for mainPage view
 */
@Controller
public class MainPageController {
	
	@GetMapping("/mainpage")
	public String getMainPage(Model model) throws IOException {		
		return "mainPage";
	}
}
