package ar.comviva.exercisethree.storage;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author lester
 * Interface for uploaded files handling
 */
public interface StorageService {
	//void init();
	/**
	 * @param file Uploaded file
	 * @return The file path where was stored in the server
	 */
	String store(MultipartFile file) throws IOException;

	/**
	 * @param filePath The path file to delete
	 * @throws IOException 
	 */
	void delete(String filePath) throws IOException;	
		
}
