package ar.comviva.exercisethree.storage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author lester
 * Handles the uploaded files administration from the server side 
 */
@Component
public class StorageServiceBean implements StorageService {
	private static String TEMP_ROOT_FOLDER = "temp//";	
	
	public StorageServiceBean() {}

	@Override
	public String store(MultipartFile file) throws IOException {
		byte[] bytes = file.getBytes();
		
        Path path = Paths.get(file.getOriginalFilename());
        
        Files.write(path, bytes);
        
        return path.toString();
	}

	@Override
	public void delete(String filePath) throws IOException {
		Files.delete(Paths.get(filePath));		
	}
	
}
