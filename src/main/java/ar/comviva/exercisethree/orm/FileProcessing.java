package ar.comviva.exercisethree.orm;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author lester
 * Entity class for FileProcessing
 */

@Entity
@Table(name = "file_processing")
public class FileProcessing {
	@Id
	@Column(name = "file_processing_id")
	private String id;
	
	@Column(name = "file_name")
	private String fileName;
	
	@Column(name = "file_value")
	private float fileValue;
	
	@Column(name = "process_date")
	private Timestamp processDate;
	
	public FileProcessing() {}
	
	public FileProcessing(String id, String fileName, float fileValue, Timestamp processDate) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.fileValue = fileValue;
		this.processDate = processDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public float getFileValue() {
		return fileValue;
	}

	public void setFileValue(float fileValue) {
		this.fileValue = fileValue;
	}

	public Timestamp getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Timestamp processDate) {
		this.processDate = processDate;
	}
	
	@Override
    public String toString() {
        return "FileProcessing{" +
                "id='" + id + "'" +
                ", fileName='" + fileName + '\'' +
                ", fileValue='" + fileValue + '\'' +
                ", processDate='" + processDate + '\'' +
                '}';
    }
}
