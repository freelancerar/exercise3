package ar.comviva.exercisethree.orm;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileProcessingRepository extends JpaRepository<FileProcessing, String>{
	 
}
