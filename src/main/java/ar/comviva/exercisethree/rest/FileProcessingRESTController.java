package ar.comviva.exercisethree.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.comviva.exercisethree.file.FileProcessor;
import ar.comviva.exercisethree.file.ResultsDTO;
import ar.comviva.exercisethree.orm.FileProcessing;
import ar.comviva.exercisethree.service.FileProcessingService;
import ar.comviva.exercisethree.service.FileProcessingServiceJDBC;
import ar.comviva.exercisethree.storage.StorageService;

/**
 * @author lester
 * REST-Controller class for the File Processing logic
 */
@RestController
public class FileProcessingRESTController {
	private final String successfulyUploadIMSG = "The system uploaded and processed the file ";
	private final String successfulyUploadIIMSG = " succesfully";
	private final String unsuccessfulyUploadIIMSG = "There was an error uploading the file";
	
	@Autowired
	FileProcessingServiceJDBC fileProcessingServiceJDBC;
	
	@Autowired
	FileProcessingService fileProcessingService; //only created to show the JPA capabilites, not used
	
	
	private final StorageService storageService;
	
	@Autowired
	public FileProcessingRESTController(StorageService storageService) {
		this.storageService = storageService;
	}
	
	@GetMapping("/storedFileProcessing")
	public List<FileProcessing> index(){
        return fileProcessingServiceJDBC.getAllFileProcessingRecords();
    }
	
	@PostMapping("/uploadfile")
	public String handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
		String message = "";
		
		try {
			String fileName = storageService.store(file);
			
			ResultsDTO results = FileProcessor.extractAndProcessNumbersFromFile(fileName);
			
			fileProcessingServiceJDBC.insertNewFileProcessing(fileName, results.getSum());
			
			message = successfulyUploadIMSG + file.getOriginalFilename() + successfulyUploadIIMSG;
			
			storageService.delete(fileName);
		} catch (IOException e) {
			message = unsuccessfulyUploadIIMSG;
		}				

		return message;
	}		
}
