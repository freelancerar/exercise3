package ar.comviva.exercisethree.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.comviva.exercisethree.orm.FileProcessing;
import ar.comviva.exercisethree.orm.FileProcessingRepository;

/**
 * @author lester
 * Service for File Processing logic
 */
@Service
public class FileProcessingServiceImpl implements FileProcessingService{
	@Autowired
	FileProcessingRepository fileProcessingRepository;
	
	@Override
	public List<FileProcessing> getAllFileProcessingRecords(){
        return fileProcessingRepository.findAll();
    }

	@Override
	public int insertNewFileProcessing(String fileName, float result) {
		// TODO Auto-generated method stub
		return 0;
	}
}
