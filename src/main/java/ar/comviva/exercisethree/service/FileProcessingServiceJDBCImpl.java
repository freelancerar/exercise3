package ar.comviva.exercisethree.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.comviva.exercisethree.db.FileProcessingDao;
import ar.comviva.exercisethree.orm.FileProcessing;

@Service
public class FileProcessingServiceJDBCImpl implements FileProcessingServiceJDBC {
	@Autowired
	FileProcessingDao processingDao;
	
	@Override
	public void insertNewFileProcessing(String fileName, float result) {
		String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		Timestamp now = Timestamp.valueOf(LocalDateTime.now());
		
		FileProcessing newRecord = new FileProcessing(uuid, fileName, result, now);
		
		processingDao.insert(newRecord);
	}

	@Override
	public List<FileProcessing> getAllFileProcessingRecords() {		
		return processingDao.getAll();
	}

}
