package ar.comviva.exercisethree.service;

import java.util.List;

import ar.comviva.exercisethree.orm.FileProcessing;

/**
 * @author lester
 * Service Interface for File Processing logic using a JDBC datasource
 */
public interface FileProcessingServiceJDBC {
	public void insertNewFileProcessing(String fileName, float result);
	
	public List<FileProcessing> getAllFileProcessingRecords();
}
