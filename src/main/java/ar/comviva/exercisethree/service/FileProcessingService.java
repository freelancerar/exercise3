package ar.comviva.exercisethree.service;

import java.util.List;

import ar.comviva.exercisethree.orm.FileProcessing;

/**
 * @author lester
 * Service Interface for File Processing logic
 */
public interface FileProcessingService {		
	public int insertNewFileProcessing(String fileName, float result);
	
	public List<FileProcessing> getAllFileProcessingRecords();
}
